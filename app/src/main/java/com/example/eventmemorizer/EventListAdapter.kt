package com.example.eventmemorizer

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

class EventListAdapter(
    private val inflater: LayoutInflater,
    private val onEventListener: OnEventListener,
): ListAdapter<EventEntity, EventViewHolder>(EventDiffer) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        val view = inflater.inflate(R.layout.event_item, parent, false)
        return EventViewHolder(view, onEventListener)
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.bindTo(position, getItem(position))
    }

    private object EventDiffer: DiffUtil.ItemCallback<EventEntity>() {
        override fun areContentsTheSame(oldItem: EventEntity, newItem: EventEntity): Boolean {
            return oldItem.title == newItem.title &&
                    oldItem.description == newItem.description
        }

        override fun areItemsTheSame(oldItem: EventEntity, newItem: EventEntity): Boolean {
            return oldItem.title == newItem.title
        }
    }

}