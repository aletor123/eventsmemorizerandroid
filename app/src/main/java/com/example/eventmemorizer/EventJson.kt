package com.example.eventmemorizer


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class EventJson(
    @Json(name = "title")
    val title: String,
    @Json(name = "description")
    val description: String,
    @Json(name = "lon")
    val lon: Double,
    @Json(name = "lat")
    val lat: Double,
    @Json(name = "id")
    val id: UUID = UUID.randomUUID(),
)