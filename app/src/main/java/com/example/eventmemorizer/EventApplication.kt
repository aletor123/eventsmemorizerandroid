package com.example.eventmemorizer

import android.app.Application

class EventApplication : Application() {
    // Using by lazy so the database and the repository are only created when they're needed
    // rather than when the application starts
    val database by lazy { EventDataBase.getDatabase(this) }
    val repository by lazy { EventRepository(database.eventDao()) }
}