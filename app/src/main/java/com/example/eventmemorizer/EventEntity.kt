package com.example.eventmemorizer

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "event_table")
class EventEntity(
        @ColumnInfo(name="title") var title: String = "",
        @ColumnInfo(name="description") var description: String = "",
        @ColumnInfo(name="long") var lon: Double = 0.0,
        @ColumnInfo(name="lat") var lat: Double = 0.0,
        @PrimaryKey()
        @ColumnInfo(name = "id")
        var id: UUID = UUID.randomUUID()
) {

}