package com.example.eventmemorizer

import android.content.Intent
import android.os.Bundle
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlin.properties.Delegates

class MapActivity: AppCompatActivity(), OnMapReadyCallback {
    lateinit var choisenLatLng : LatLng
    private var is_map_getter by Delegates.notNull<Boolean>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        val choiceLocationButton = findViewById<Button>(R.id.choice_location_button)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)
        is_map_getter = intent.getBooleanExtra(IS_MAP_GETTER, true)

        if (!is_map_getter){
            choiceLocationButton.visibility = INVISIBLE
            val view = mapFragment?.view
            view?.isClickable = false
        }else{
            choiceLocationButton.visibility = VISIBLE
        }

        choiceLocationButton.setOnClickListener {
            val intent = Intent()
            intent.putExtra(CHOSEN_LAT, choisenLatLng.latitude)
            intent.putExtra(CHOSEN_LON, choisenLatLng.longitude)
            setResult(RESULT_OK, intent)
            finish()
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        if (!is_map_getter) {
            val lon = intent.getDoubleExtra(CHOSEN_LON, 0.0)
            val lat = intent.getDoubleExtra(CHOSEN_LAT, 0.0)
            googleMap?.apply {
                val mark = LatLng(lat, lon)
                addMarker(
                    MarkerOptions()
                        .position(mark)
                )
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mark, 12F));
            }
        }else{
            googleMap?.setOnMapClickListener (GoogleMap.OnMapClickListener {
                googleMap.clear();
                googleMap.apply {
                    addMarker(
                        MarkerOptions()
                            .position(it)
                    )
                }
                choisenLatLng = it
            })
        }
    }

    companion object{
        const val IS_MAP_GETTER = "is_map_getter"
        const val CHOSEN_LON = "CHOSEN_LON"
        const val CHOSEN_LAT = "CHOSEN_LAT"
    }
}