package com.example.eventmemorizer

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class EventDetailsActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_details)
        val titleView = findViewById<TextView>(R.id.event_details_title)
        val descView = findViewById<TextView>(R.id.event_details_description)
        val mapButton = findViewById<Button>(R.id.details_location_button)

        titleView.text = intent.getStringExtra(MainActivity.CHOICE_EVENT_TITLE)
        descView.text = intent.getStringExtra(MainActivity.CHOICE_EVENT_DESC)

        val chosenLon = intent.getDoubleExtra(MapActivity.CHOSEN_LON, 0.0)
        val chosenLat = intent.getDoubleExtra(MapActivity.CHOSEN_LAT, 0.0)

        mapButton.setOnClickListener {
            val intent = Intent(applicationContext, MapActivity::class.java)
            intent.putExtra(MapActivity.IS_MAP_GETTER, false)
            intent.putExtra(MapActivity.CHOSEN_LON, chosenLon)
            intent.putExtra(MapActivity.CHOSEN_LAT, chosenLat)
            startActivity(intent)
        }
    }
}
