package com.example.eventmemorizer

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class AddNewEventActivity: AppCompatActivity() {
    var chosenLat = 0.0
    var chosenLon = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_event)

        val titleView = findViewById<TextView>(R.id.new_event_title)
        val descView = findViewById<TextView>(R.id.new_event_description)
        val mapButton = findViewById<Button>(R.id.add_location_button)
        val newEventButton = findViewById<Button>(R.id.add_new_event_button)
        val saveLocalyCheckBox = findViewById<CheckBox>(R.id.add_new_event_save_localy)
//        val curIntent = intent
//        curIntent.getStringExtra()
        newEventButton.setOnClickListener {
            val intent = Intent()
            intent.putExtra(MainActivity.NEW_EVENT_TITLE, titleView.text.toString())
            intent.putExtra(MainActivity.NEW_EVENT_DESC, descView.text.toString())
            intent.putExtra(MainActivity.NEW_EVENT_SAVE_LOCALY, saveLocalyCheckBox.isChecked)
            intent.putExtra(MainActivity.NEW_EVENT_LON, chosenLon)
            intent.putExtra(MainActivity.NEW_EVENT_LAT, chosenLat)
            setResult(RESULT_OK, intent)
            finish()
        }

        mapButton.setOnClickListener {
            val intent = Intent(applicationContext, MapActivity::class.java)
            startActivityForResult(intent, CHOICE_PLACE_REQUEST_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CHOICE_PLACE_REQUEST_CODE -> {
                if (data != null) {
                    chosenLat = data.getDoubleExtra(MapActivity.CHOSEN_LAT, 0.0)
                    chosenLon = data.getDoubleExtra(MapActivity.CHOSEN_LON, 0.0)
                }
            }
        }
    }

    companion object {
        const val CHOICE_PLACE_REQUEST_CODE = 1

    }
}