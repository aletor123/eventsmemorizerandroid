package com.example.eventmemorizer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import org.json.JSONArray
import org.json.JSONObject
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

class MainActivity : AppCompatActivity(), OnEventListener {

    private val eventViewModel: EventViewModel by viewModels {
        EventViewModelFactory((application as EventApplication).repository)
    }

    lateinit var eventAdapter: EventListAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val eventList = findViewById<RecyclerView>(R.id.events_list)
        eventList.layoutManager = LinearLayoutManager(applicationContext)
        eventAdapter = EventListAdapter(layoutInflater, this)
        eventList.adapter = eventAdapter
        eventList
            .addItemDecoration(DividerItemDecoration(applicationContext,
                DividerItemDecoration.VERTICAL))

        eventViewModel.allEvents.observe(this, { events ->
            // Update the cached copy of the words in the adapter.
            events?.let { eventAdapter.submitList(it) }
        })

        val addEventButton = findViewById<Button>(R.id.add_button)
        addEventButton.setOnClickListener {
            val intent = Intent(applicationContext, AddNewEventActivity::class.java)
            startActivityForResult(intent, ADD_NEW_EVENT_REQUEST_CODE)
        }
    }

    override fun onStart() {
        super.onStart()
        getEventsFromApi()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            ADD_NEW_EVENT_REQUEST_CODE -> {
                if (resultCode == RESULT_OK && data != null) {
                    val title = data.getStringExtra(NEW_EVENT_TITLE)
                    val desc = data.getStringExtra(NEW_EVENT_DESC)
                    val lon = data.getDoubleExtra(NEW_EVENT_LON, 0.0)
                    val lat = data.getDoubleExtra(NEW_EVENT_LAT, 0.0)
                    val saveLocaly = data.getBooleanExtra(NEW_EVENT_SAVE_LOCALY, true)
                    if (title != null && desc != null) {
                        if (saveLocaly){
                            val entity = EventEntity(title, desc, lon, lat)
                            eventViewModel.insert(entity)
                        }else {
                            val entity = EventJson(title, desc, lon, lat)
                            createEventInApi(entity)
                        }
                    }
                }
            }
        }
    }

    override fun onEventClick(position: Int) {
        val clickedEvent = eventViewModel.allEvents.value?.get(position)
        if (clickedEvent != null) {
            val intent = Intent(applicationContext, EventDetailsActivity::class.java)
            intent.putExtra(CHOICE_EVENT_TITLE, clickedEvent.title)
            intent.putExtra(CHOICE_EVENT_DESC, clickedEvent.description)
            intent.putExtra(MapActivity.CHOSEN_LON, clickedEvent.lon)
            intent.putExtra(MapActivity.CHOSEN_LAT, clickedEvent.lat)
            startActivity(intent)
        }
    }

    private fun createEventInApi(event: EventJson) {
        val queue = Volley.newRequestQueue(this)
        val jsonAdapter: JsonAdapter<EventJson> = Moshi.Builder().addLast(UUIDAdapter()).build().adapter(EventJson::class.java)
        val jsonStr = jsonAdapter.toJson(event)
        val url = "http://10.0.2.2:8000/api/events/"
        Toast.makeText(applicationContext, event.title.toString(), Toast.LENGTH_LONG).show()
        val customRequest = JsonObjectRequest(
            Request.Method.POST, url, JSONObject(jsonStr),
            { response -> handleCreateResponse(response) },
            {})
        queue.add(customRequest)
    }

    private fun handleCreateResponse(response: JSONObject){
        val jsonAdapter: JsonAdapter<EventJson> = Moshi.Builder().addLast(UUIDAdapter()).build().adapter(EventJson::class.java)
        val event = jsonAdapter.fromJson(response.toString())
        if (event != null) {
            val eventEntity = EventEntity(
                title = event.title,
                description = event.description,
                lon = event.lon,
                lat = event.lat,
                id = event.id,
            )
            eventViewModel.insert(eventEntity)
        }
    }


    private fun getEventsFromApi() {
        val queue = Volley.newRequestQueue(this)

        val url = "http://10.0.2.2:8000/api/events/"

        val customRequest = JsonArrayRequest(
            Request.Method.GET, url, null,
            { response -> handleListResponse(response) },
            {})

        queue.add(customRequest)
    }

    /**
     * Handle [response] and populate search list if response is correct
     */
    private fun handleListResponse(response: JSONArray) {
        val jsonAdapter: JsonAdapter<EventJson> = Moshi.Builder()
            .addLast(UUIDAdapter()).build().adapter(EventJson::class.java)
        for (index in 0..response.length()) {
            val event = response.optJSONObject(index)
            event?.let { jsonObject ->
                jsonAdapter.fromJson(jsonObject.toString())?.let {
                    EventEntity(
                        id=it.id,
                        title=it.title,
                        description=it.description,
                        lon=it.lon,
                        lat=it.lat,
                    )
                }

            }?.let {
                eventViewModel.insert(it)
            }
        }
    }

    companion object {
        const val ADD_NEW_EVENT_REQUEST_CODE = 1
        const val EVENT_DETAILS_REQUEST_CODE = 2

        const val CHOICE_EVENT_TITLE = "choiceEventTitle"
        const val CHOICE_EVENT_DESC = "choiceEventDesc"
        const val NEW_EVENT_TITLE = "newEventTitle"
        const val NEW_EVENT_DESC = "newEventDesc"
        const val NEW_EVENT_SAVE_LOCALY = "newEventSaveLocaly"
        const val NEW_EVENT_LON = "newEventLon"
        const val NEW_EVENT_LAT = "newEventLat"
    }
}