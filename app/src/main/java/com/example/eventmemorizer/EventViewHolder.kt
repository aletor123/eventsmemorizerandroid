package com.example.eventmemorizer

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class EventViewHolder(
    private val view: View,
    private val onEventListener: OnEventListener
): RecyclerView.ViewHolder(view), View.OnClickListener {
    fun bindTo(position: Int, eventEntity: EventEntity) {
        val title = view.findViewById<TextView>(R.id.event_title)
        val description = view.findViewById<TextView>(R.id.event_description)
        title.text = eventEntity.title
        description.text = eventEntity.description
        view.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        onEventListener.onEventClick(adapterPosition)
    }
}