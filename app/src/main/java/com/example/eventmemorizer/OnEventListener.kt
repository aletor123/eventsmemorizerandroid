package com.example.eventmemorizer

interface OnEventListener {
    fun onEventClick(position: Int)
}